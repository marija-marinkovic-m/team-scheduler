// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const electron = require('electron');
const remote = electron.remote;

    
    
    
    function domReady() {
        let webContents = remote.getCurrentWebContents();
        
        webContents.on('dom-ready', function() {
            webContents.executeJavaScript("document.getElementById('reload-app').addEventListener('click', function() {console.log('clicked')})", true);
        });
    }
    
    
    domReady();

