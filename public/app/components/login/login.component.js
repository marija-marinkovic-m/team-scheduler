"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var index_1 = require('../../auth/index');
var index_2 = require('../../shared/index');
var LoginComponent = (function () {
    function LoginComponent(_router, _auth) {
        this._router = _router;
        this._auth = _auth;
        this.msg = [];
    }
    LoginComponent.prototype.onLogin = function (event) {
        var _this = this;
        this.msg = [];
        this._auth.login(event).then(function (res) {
            _this._router.navigate(['Dashboard']);
        }, function (err) {
            _this.msg.push({ severity: 'error', summary: 'Login failed', detail: err });
        });
    };
    LoginComponent.prototype.onRegister = function (event) {
        //this._router.navigate(['Register']);
    };
    LoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-login',
            template: "\n\t\t<entry-form (loginSubmit)=\"onLogin($event)\" [responseMsg]=\"msg\"></entry-form>\n\t\t",
            directives: [index_2.EntryFormDirective]
        }),
        router_deprecated_1.CanActivate(function (next, prev) {
            return index_1.authCheck(next, prev);
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, index_1.Auth])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map