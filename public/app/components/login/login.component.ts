import { Component } from '@angular/core';
import { Router, ComponentInstruction, CanActivate } from '@angular/router-deprecated';

import { Message } from 'primeng/primeng';
import { Auth, authCheck } from '../../auth/index';
import { EntryFormDirective } from '../../shared/index';

@Component({
	moduleId: module.id, 
	selector: 'app-login',
	template: `
		<entry-form (loginSubmit)="onLogin($event)" [responseMsg]="msg"></entry-form>
		`,
	directives: [EntryFormDirective]
})
@CanActivate((next:ComponentInstruction, prev:ComponentInstruction) => {
	return authCheck(next, prev);
})
export class LoginComponent {
	msg: Message[] = [];
	
	constructor(
		private _router: Router, 
		private _auth: Auth
	) { }
	
	onLogin(event: FirebaseCredentials) {
		this.msg = [];
		this._auth.login(event).then(
			res => {
				this._router.navigate(['Dashboard']);
			}, 
			err => {
				this.msg.push({severity: 'error', summary: 'Login failed', detail: err});
			}
		)
	}
	onRegister(event: string) {
		//this._router.navigate(['Register']);
	}
	
}