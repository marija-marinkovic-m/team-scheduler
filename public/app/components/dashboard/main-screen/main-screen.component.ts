import { Component, OnInit, Renderer } from '@angular/core';
import { FORM_DIRECTIVES, FormBuilder, ControlGroup, Control, NgForm } from '@angular/common';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { Tooltip, Calendar, InputText, Message, Growl } from 'primeng/primeng';
import { ValuesPipe, AvatarPipe } from '../../../shared/index';

import * as moment_ from 'moment';
const moment: moment.MomentStatic = (<any>moment_)['default'] || moment_;

@Component({
    moduleId: module.id, 
    selector: 'dashboard-main-screen',
    templateUrl: 'main-screen.component.html', 
    directives: [FORM_DIRECTIVES, Tooltip, Calendar, InputText, Growl], 
    pipes: [ValuesPipe, AvatarPipe]
})
export class MainScreenComponent implements OnInit {
    addPersonForm: ControlGroup;
    workers: FirebaseListObservable<any>;
    isResetting: boolean = false;
    msgs: Message[] = [];
    today: moment.Moment = moment();
    
    constructor(
        private _fb: FormBuilder, 
        private _af: AngularFire, 
        private _render: Renderer
    ) {
        this.workers = this._af.database.list('/people');
    }

    ngOnInit() {
        this._buildAddPersonForm();
    }
    
    _buildAddPersonForm():void {
        this.addPersonForm = this._fb.group({
            name: new Control('')
        });
    }
    
    addPerson():void {
        this.workers.push(this.addPersonForm.value).then(r => {
                this.msgs.push({severity: 'info', summary: 'Success', detail: 'Person data added.'});
                
                this._buildAddPersonForm();
                this.isResetting = true;
                setTimeout(() => this.isResetting = false, 0);
            }).catch(e => {
                this.msgs.push({severity: 'error', summary: 'Error', detail: 'Person data creation failed. Please try later.'})
            });
    }
    updatePerson(workerKey: string, workerEditForm:any, formHolderEl: HTMLElement):void {
        if (workerEditForm.valid) {
            this.workers.update(workerKey, workerEditForm.value).then(r => {
                this.msgs.push({severity: 'info', summary: 'Success', detail: 'Person data updated.'});
                this._render.setElementClass(formHolderEl, 'opened', false);
            }).catch(e => {
                this.msgs.push({severity: 'error', summary: 'Error', detail: 'Update failed. Please try later.'})
            });
        } else {
            this.msgs.push({severity: 'warn', summary: 'Warning', detail: 'Name is required field.'});
        }
    }
    removePerson(workerKey: string):void {
        if (confirm('Are you sure you want to delete this person\'s data? This cann\'t be undone.')) {
            this.workers.remove(workerKey).then(r => {
                this.msgs.push({severity: 'info', summary: 'Success', detail: 'Person data removed.'});
            }).catch(e => {
                this.msgs.push({severity: 'error', summary: 'Error', detail: 'Person data deletion failed. Please try later.'})
            });  
        }
    }
    
    addProject(workerProject: NgForm, workerKey: string):void {    
        this._af.database.list('/people/' + workerKey + '/projects').push(workerProject.value).then(r => {
                this.msgs.push({severity: 'info', summary: 'Success', detail: 'Project added.'});
            }).catch(e => {
                this.msgs.push({severity: 'error', summary: 'Error', detail: 'Project creation failed. Please try later.'})
            });
    }
    updateProject(formData: NgForm, workerKey: string, projectKey: string):void {
        if (formData.valid) {
            this._af.database.object(`/people/${workerKey}/projects/${projectKey}`).update(formData.value).then(r => {
                this.msgs.push({severity: 'info', summary: 'Success', detail: 'Project updated.'});
            }).catch(e => {
                this.msgs.push({severity: 'error', summary: 'Error', detail: 'Update failed. Please try later.'})
            });
        } else {
            this.msgs.push({severity: 'warn', summary: 'Warning', detail: 'All fields are required.'});
        }
    }
    removeProject(workerKey: string, projectKey: string):void {
        if (confirm('Are you sure you want to delete this project\' data? This can not be undone.')) {
            this._af.database.object(`/people/${workerKey}/projects/${projectKey}`).remove().then(r => {
                this.msgs.push({severity: 'info', summary: 'Success', detail: 'Project deleted.'});
            }).catch(e => {
                this.msgs.push({severity: 'error', summary: 'Error', detail: 'Project deletion failed. Please try later.'})
            });
        }
    }    
    toggleFormEl(formEl: HTMLElement):void {
        let isAdd: boolean = formEl.className !== 'opened';
        this._render.setElementClass(formEl, 'opened', isAdd);
        if (isAdd) {
            formEl.getElementsByTagName('input')[0].focus();
        }
    }
    onKeyDown(event: any, formHolderEl: any = false):void {
        let isEscape = false;
        if ("key" in event) {
            isEscape = event.key == "Escape";
        } else {
            isEscape = event.keyCode == 27;
        }
        if (isEscape) {
            if (formHolderEl !== false) {
                this._render.setElementClass(formHolderEl, 'opened', false);
            }
        }
    }
    isPast(date: string):boolean {
        return this.today.isAfter(date);
    }
    isExpiryPeriod(date: string):boolean {
        return moment().add('days', 7).isAfter(date) && !this.isPast(date);
    }
}