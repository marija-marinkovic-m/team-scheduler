"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var angularfire2_1 = require('angularfire2');
var primeng_1 = require('primeng/primeng');
var index_1 = require('../../../shared/index');
var moment_ = require('moment');
var moment = moment_['default'] || moment_;
var MainScreenComponent = (function () {
    function MainScreenComponent(_fb, _af, _render) {
        this._fb = _fb;
        this._af = _af;
        this._render = _render;
        this.isResetting = false;
        this.msgs = [];
        this.today = moment();
        this.workers = this._af.database.list('/people');
    }
    MainScreenComponent.prototype.ngOnInit = function () {
        this._buildAddPersonForm();
    };
    MainScreenComponent.prototype._buildAddPersonForm = function () {
        this.addPersonForm = this._fb.group({
            name: new common_1.Control('')
        });
    };
    MainScreenComponent.prototype.addPerson = function () {
        var _this = this;
        this.workers.push(this.addPersonForm.value).then(function (r) {
            _this.msgs.push({ severity: 'info', summary: 'Success', detail: 'Person data added.' });
            _this._buildAddPersonForm();
            _this.isResetting = true;
            setTimeout(function () { return _this.isResetting = false; }, 0);
        }).catch(function (e) {
            _this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Person data creation failed. Please try later.' });
        });
    };
    MainScreenComponent.prototype.updatePerson = function (workerKey, workerEditForm, formHolderEl) {
        var _this = this;
        if (workerEditForm.valid) {
            this.workers.update(workerKey, workerEditForm.value).then(function (r) {
                _this.msgs.push({ severity: 'info', summary: 'Success', detail: 'Person data updated.' });
                _this._render.setElementClass(formHolderEl, 'opened', false);
            }).catch(function (e) {
                _this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Update failed. Please try later.' });
            });
        }
        else {
            this.msgs.push({ severity: 'warn', summary: 'Warning', detail: 'Name is required field.' });
        }
    };
    MainScreenComponent.prototype.removePerson = function (workerKey) {
        var _this = this;
        if (confirm('Are you sure you want to delete this person\'s data? This cann\'t be undone.')) {
            this.workers.remove(workerKey).then(function (r) {
                _this.msgs.push({ severity: 'info', summary: 'Success', detail: 'Person data removed.' });
            }).catch(function (e) {
                _this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Person data deletion failed. Please try later.' });
            });
        }
    };
    MainScreenComponent.prototype.addProject = function (workerProject, workerKey) {
        var _this = this;
        this._af.database.list('/people/' + workerKey + '/projects').push(workerProject.value).then(function (r) {
            _this.msgs.push({ severity: 'info', summary: 'Success', detail: 'Project added.' });
        }).catch(function (e) {
            _this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Project creation failed. Please try later.' });
        });
    };
    MainScreenComponent.prototype.updateProject = function (formData, workerKey, projectKey) {
        var _this = this;
        if (formData.valid) {
            this._af.database.object("/people/" + workerKey + "/projects/" + projectKey).update(formData.value).then(function (r) {
                _this.msgs.push({ severity: 'info', summary: 'Success', detail: 'Project updated.' });
            }).catch(function (e) {
                _this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Update failed. Please try later.' });
            });
        }
        else {
            this.msgs.push({ severity: 'warn', summary: 'Warning', detail: 'All fields are required.' });
        }
    };
    MainScreenComponent.prototype.removeProject = function (workerKey, projectKey) {
        var _this = this;
        if (confirm('Are you sure you want to delete this project\' data? This can not be undone.')) {
            this._af.database.object("/people/" + workerKey + "/projects/" + projectKey).remove().then(function (r) {
                _this.msgs.push({ severity: 'info', summary: 'Success', detail: 'Project deleted.' });
            }).catch(function (e) {
                _this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Project deletion failed. Please try later.' });
            });
        }
    };
    MainScreenComponent.prototype.toggleFormEl = function (formEl) {
        var isAdd = formEl.className !== 'opened';
        this._render.setElementClass(formEl, 'opened', isAdd);
        if (isAdd) {
            formEl.getElementsByTagName('input')[0].focus();
        }
    };
    MainScreenComponent.prototype.onKeyDown = function (event, formHolderEl) {
        if (formHolderEl === void 0) { formHolderEl = false; }
        var isEscape = false;
        if ("key" in event) {
            isEscape = event.key == "Escape";
        }
        else {
            isEscape = event.keyCode == 27;
        }
        if (isEscape) {
            if (formHolderEl !== false) {
                this._render.setElementClass(formHolderEl, 'opened', false);
            }
        }
    };
    MainScreenComponent.prototype.isPast = function (date) {
        return this.today.isAfter(date);
    };
    MainScreenComponent.prototype.isExpiryPeriod = function (date) {
        return moment().add('days', 7).isAfter(date) && !this.isPast(date);
    };
    MainScreenComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'dashboard-main-screen',
            templateUrl: 'main-screen.component.html',
            directives: [common_1.FORM_DIRECTIVES, primeng_1.Tooltip, primeng_1.Calendar, primeng_1.InputText, primeng_1.Growl],
            pipes: [index_1.ValuesPipe, index_1.AvatarPipe]
        }), 
        __metadata('design:paramtypes', [common_1.FormBuilder, angularfire2_1.AngularFire, core_1.Renderer])
    ], MainScreenComponent);
    return MainScreenComponent;
}());
exports.MainScreenComponent = MainScreenComponent;
//# sourceMappingURL=main-screen.component.js.map