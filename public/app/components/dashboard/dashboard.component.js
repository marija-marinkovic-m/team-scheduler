"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var index_1 = require('../../auth/index');
var index_2 = require('./main-screen/index');
var primeng_1 = require('primeng/primeng');
var DashboardComponent = (function () {
    function DashboardComponent(_auth, _renderer) {
        this._auth = _auth;
        this._renderer = _renderer;
        this.mobileMenuActive = false;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.loggedIn = this._auth.state();
        if (this.loggedIn) {
            this.authMail = this._auth.getData().password.email;
        }
        this._renderer.setElementStyle(document.getElementById('main'), 'background', '#fff');
    };
    DashboardComponent.prototype.toggleMenu = function (e) {
        this.mobileMenuActive = !this.mobileMenuActive;
        e.preventDefault();
    };
    DashboardComponent.prototype.logout = function () {
        this._auth.logout();
        location.reload();
    };
    DashboardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-dashboard',
            templateUrl: 'dashboard.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, primeng_1.Menu, primeng_1.Button, primeng_1.Tooltip],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/',
                name: 'MainScreen',
                component: index_2.MainScreenComponent,
                useAsDefault: true
            }
        ]),
        router_deprecated_1.CanActivate(function (next, prev) {
            return index_1.authCheck(next, prev);
        }), 
        __metadata('design:paramtypes', [index_1.Auth, core_1.Renderer])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map