import { Component, OnInit, ViewEncapsulation, Renderer, RootRenderer, RenderComponentType } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, CanActivate } from '@angular/router-deprecated';

import { Auth, authCheck } from '../../auth/index';

import { MainScreenComponent } from './main-screen/index';

import { Menu, Button, Tooltip } from 'primeng/primeng';

@Component({
	moduleId: module.id, 
	selector: 'app-dashboard',
	templateUrl: 'dashboard.component.html',
	directives: [ROUTER_DIRECTIVES, Menu, Button, Tooltip], 
	encapsulation: ViewEncapsulation.None
})
@RouteConfig([
	{
		path: '/', 
		name: 'MainScreen', 
		component: MainScreenComponent, 
		useAsDefault: true
	}
])
@CanActivate((next, prev) => {
	return authCheck(next, prev);
})
export class DashboardComponent implements OnInit {
	activeMenuId: string;
	authMail: string;
	loggedIn: boolean;
	mobileMenuActive: boolean = false;
	
	constructor(
		private _auth: Auth, 
		private _renderer: Renderer
	) {}
	
	ngOnInit() {
		this.loggedIn = this._auth.state();
		if (this.loggedIn) {
			this.authMail = this._auth.getData().password.email;
		}
		
		this._renderer.setElementStyle(document.getElementById('main'), 'background', '#fff');
	}
	
	toggleMenu(e) {
        this.mobileMenuActive = !this.mobileMenuActive;
        e.preventDefault();
    }
	
	logout() {
		this._auth.logout();
		location.reload();
	}

}
