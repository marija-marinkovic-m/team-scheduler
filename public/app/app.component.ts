import { Component } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { DashboardComponent, LoginComponent } from './components/index';

@Component({
    selector: 'etl-app', 
    template: `<router-outlet></router-outlet>`, 
    directives: [ROUTER_DIRECTIVES]
})
@RouteConfig([
    {
        path: '/login', 
        name: 'Login', 
        component: LoginComponent, 
        useAsDefault: true
    }, 
    {
        path: '/dashboard/...', 
        name: 'Dashboard', 
        component: DashboardComponent
    }
])
export class EutelnetAppComponent {}