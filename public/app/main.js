"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var common_1 = require('@angular/common');
var core_1 = require('@angular/core');
var app_component_1 = require('./app.component');
var router_deprecated_1 = require('@angular/router-deprecated');
var angularfire2_1 = require('angularfire2');
var index_1 = require('./auth/index');
// enableProdMode();
platform_browser_dynamic_1.bootstrap(app_component_1.EutelnetAppComponent, [
    router_deprecated_1.ROUTER_PROVIDERS,
    core_1.provide(common_1.LocationStrategy, { useClass: common_1.HashLocationStrategy }),
    angularfire2_1.FIREBASE_PROVIDERS,
    angularfire2_1.defaultFirebase('https://amber-fire-98.firebaseio.com/team-app/'),
    angularfire2_1.firebaseAuthConfig({
        provider: angularfire2_1.AuthProviders.Password,
        method: angularfire2_1.AuthMethods.Password
    }),
    index_1.Auth
]).then(function (appRef) {
    index_1.appInjector(appRef.injector);
}, function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map