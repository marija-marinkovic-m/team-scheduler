import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'values'
})
export class ValuesPipe implements PipeTransform {
    transform(value: any, args: any[]): any {
        let keyArr = Object.keys(value), 
            dataArr = [];
            
        // loop trough the object 
        // pushing values to the return array 
        keyArr.forEach(key => {
            let currObj = value[key];
            currObj['$key'] = key;
            dataArr.push(currObj);
        });
        
        return dataArr;
    }
}

@Pipe({
    name: 'avatar'
})
export class AvatarPipe implements PipeTransform {
    colours:string[] = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#f1c40f", "#e67e22", "#e74c3c", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"];
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    canvasWidth: number = 36;
    canvasHeight: number = 36;
    
    transform(value: HTMLCanvasElement): any {
        
        this.canvas = value;
        this.context = this.canvas.getContext('2d');
        
        // set width and height
        this.canvas.width = this.canvasWidth;
        this.canvas.height = this.canvasHeight;
        
        let workerName = this.canvas.getAttribute('data-name');
        let fontSize = this.canvasHeight / 2;
        
        // apply generated avatar 
        this.context.fillStyle = this.colours[this.getColourIndex(workerName)];
        this.context.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.context.font = `${fontSize}px Arial`;
        this.context.textAlign = "center";
        this.context.fillStyle = "#FFF";
        this.context.fillText(this.getInitials(workerName), this.canvasWidth / 2, this.canvasHeight / 1.5);
        return workerName;
    }
    
    getInitials(val:string):string {
        let nameSplit = val.split(" ");
        return nameSplit[0].charAt(0).toUpperCase() + nameSplit[1].charAt(0).toUpperCase();
    }
    getColourIndex(val:string):number {
        let charIndex = val.trim().charCodeAt(0) - 65;
        return charIndex % 19;
    }
}