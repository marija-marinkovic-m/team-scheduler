import { Control } from '@angular/common';

export class CustomValidators {
    static emailFormat(control: Control) {
        let pattern:RegExp = /\S+@\S+\.\S+/;
        return pattern.test(control.value) ? null : {"emailFormat": true};
    }
}