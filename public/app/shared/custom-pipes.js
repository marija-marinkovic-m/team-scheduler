"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ValuesPipe = (function () {
    function ValuesPipe() {
    }
    ValuesPipe.prototype.transform = function (value, args) {
        var keyArr = Object.keys(value), dataArr = [];
        // loop trough the object 
        // pushing values to the return array 
        keyArr.forEach(function (key) {
            var currObj = value[key];
            currObj['$key'] = key;
            dataArr.push(currObj);
        });
        return dataArr;
    };
    ValuesPipe = __decorate([
        core_1.Pipe({
            name: 'values'
        }), 
        __metadata('design:paramtypes', [])
    ], ValuesPipe);
    return ValuesPipe;
}());
exports.ValuesPipe = ValuesPipe;
var AvatarPipe = (function () {
    function AvatarPipe() {
        this.colours = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#f1c40f", "#e67e22", "#e74c3c", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"];
        this.canvasWidth = 36;
        this.canvasHeight = 36;
    }
    AvatarPipe.prototype.transform = function (value) {
        this.canvas = value;
        this.context = this.canvas.getContext('2d');
        // set width and height
        this.canvas.width = this.canvasWidth;
        this.canvas.height = this.canvasHeight;
        var workerName = this.canvas.getAttribute('data-name');
        var fontSize = this.canvasHeight / 2;
        // apply generated avatar 
        this.context.fillStyle = this.colours[this.getColourIndex(workerName)];
        this.context.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.context.font = fontSize + "px Arial";
        this.context.textAlign = "center";
        this.context.fillStyle = "#FFF";
        this.context.fillText(this.getInitials(workerName), this.canvasWidth / 2, this.canvasHeight / 1.5);
        return workerName;
    };
    AvatarPipe.prototype.getInitials = function (val) {
        var nameSplit = val.split(" ");
        return nameSplit[0].charAt(0).toUpperCase() + nameSplit[1].charAt(0).toUpperCase();
    };
    AvatarPipe.prototype.getColourIndex = function (val) {
        var charIndex = val.trim().charCodeAt(0) - 65;
        return charIndex % 19;
    };
    AvatarPipe = __decorate([
        core_1.Pipe({
            name: 'avatar'
        }), 
        __metadata('design:paramtypes', [])
    ], AvatarPipe);
    return AvatarPipe;
}());
exports.AvatarPipe = AvatarPipe;
//# sourceMappingURL=custom-pipes.js.map