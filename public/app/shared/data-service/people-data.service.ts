import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

Injectable()
export class PeopleDataService {
    items: FirebaseListObservable<any>;
    constructor(
        private _af: AngularFire
    ) {
        this.items = this._af.database.list('/people');
    }
    add(data: any) {
        return this.items.push(data);
    }
    update(key: string, data: any) {
        return this.items.update(key, data);
    }
    deleteOne(key: string) {
        return this.items.remove(key);
    }
    deleteAll() {
        return this.items.remove();
    }
    
    getOne(key: string):FirebaseObjectObservable<any> {
        return this._af.database.object('/people/' + key);
    }
}