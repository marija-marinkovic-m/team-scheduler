import { Injectable } from '@angular/core';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

@Injectable()
export class DataService {
    items: FirebaseListObservable<any>;
    
    constructor() {}
    
    add(data: any) {
        return this.items.push(data);
    }
    update(key: string, data: any) {
        return this.items.update(key, data);
    }
    deleteOne(key: string) {
        return this.items.remove(key);
    }
    deleteAll() {
        return this.items.remove();
    }
    
}