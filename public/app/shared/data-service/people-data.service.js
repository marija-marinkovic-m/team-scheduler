"use strict";
var core_1 = require('@angular/core');
core_1.Injectable();
var PeopleDataService = (function () {
    function PeopleDataService(_af) {
        this._af = _af;
        this.items = this._af.database.list('/people');
    }
    PeopleDataService.prototype.add = function (data) {
        return this.items.push(data);
    };
    PeopleDataService.prototype.update = function (key, data) {
        return this.items.update(key, data);
    };
    PeopleDataService.prototype.deleteOne = function (key) {
        return this.items.remove(key);
    };
    PeopleDataService.prototype.deleteAll = function () {
        return this.items.remove();
    };
    PeopleDataService.prototype.getOne = function (key) {
        return this._af.database.object('/people/' + key);
    };
    return PeopleDataService;
}());
exports.PeopleDataService = PeopleDataService;
//# sourceMappingURL=people-data.service.js.map