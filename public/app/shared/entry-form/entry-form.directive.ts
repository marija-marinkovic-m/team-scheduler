import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FORM_DIRECTIVES, FormBuilder, Control, ControlGroup, Validators } from '@angular/common';

import { Growl, Message, Messages } from 'primeng/primeng';

import { CustomValidators } from '../custom-validators';

@Component({ 
	selector: 'entry-form', 
	template: `
	<div class="login-page">
		<img src="app/assets/img/logo-small.png" alt="">
		<form role="form" [ngFormModel]="formGroup" (ngSubmit)="onSubmit()" novalidate>
			<div class="form-content">
				<div class="form-group">
					<input [ngFormControl]="email" type="text" placeholder="Email...">
				</div>
				<div class="form-group">
					<input [ngFormControl]="password" type="password" placeholder="Password...">
				</div>
				
				<p-messages [value]="resErrors"></p-messages>
				
			</div>
            <button type="button" (click)="onLogin()">Log in</button>
			
		</form>
	</div>
	<p-growl [value]="msgs" [sticky]="true"></p-growl>
	`, 
	directives: [FORM_DIRECTIVES, Growl, Messages]
})
export class EntryFormDirective {
	@Output('loginSubmit') onLoginEmitter: EventEmitter<{}>;
	@Input('responseMsg') resErrors: Message[];
	
  	email:Control;
	password:Control;
	formGroup:ControlGroup;
	
	msgs: Message[] = [];

	constructor(
		private _formBuilder: FormBuilder
	) {
		this.onLoginEmitter = new EventEmitter();
		this.resErrors = [];
		
		this.email = new Control('', Validators.compose([Validators.required, CustomValidators.emailFormat]));
		this.password = new Control('', Validators.compose([Validators.required, Validators.minLength(6)]));
		this.formGroup = this._formBuilder.group({
			email: this.email, 
			password: this.password
		});
	}
	
	onLogin() {
        if (this.validateForm()) {
            this.onLoginEmitter.emit(this.formGroup.value);
        }
	}
	
	validateForm():boolean {
		this.msgs = [];
		if (this.formGroup.valid) {
			return true; 
		}
		if (! this.formGroup.controls['password'].valid) {
			
			if (this.formGroup.controls['password'].hasError('required')) {
				this.msgs.push({severity: 'error', summary: 'Error', detail: 'Password is required'});
			} else {
				if (this.formGroup.controls['password'].hasError('minlength')) {
					this.msgs.push({severity: 'error', summary: 'Error', detail: 'Your password must be at least 6 characters long'})
				}
			}
			
		}
		
		if (! this.formGroup.controls['email'].valid) {
			if (this.formGroup.controls['email'].hasError('required')) {
				this.msgs.push({severity: 'error', summary: 'Error', detail: 'Email is required'});
			} else {
				if (this.formGroup.controls['email'].hasError('emailFormat')) {
					this.msgs.push({severity: 'error', summary: 'Error', detail: 'Please enter valid email'});
				}
			}
		}
		return false;
	}

}
