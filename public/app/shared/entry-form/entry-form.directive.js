"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var primeng_1 = require('primeng/primeng');
var custom_validators_1 = require('../custom-validators');
var EntryFormDirective = (function () {
    function EntryFormDirective(_formBuilder) {
        this._formBuilder = _formBuilder;
        this.msgs = [];
        this.onLoginEmitter = new core_1.EventEmitter();
        this.resErrors = [];
        this.email = new common_1.Control('', common_1.Validators.compose([common_1.Validators.required, custom_validators_1.CustomValidators.emailFormat]));
        this.password = new common_1.Control('', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(6)]));
        this.formGroup = this._formBuilder.group({
            email: this.email,
            password: this.password
        });
    }
    EntryFormDirective.prototype.onLogin = function () {
        if (this.validateForm()) {
            this.onLoginEmitter.emit(this.formGroup.value);
        }
    };
    EntryFormDirective.prototype.validateForm = function () {
        this.msgs = [];
        if (this.formGroup.valid) {
            return true;
        }
        if (!this.formGroup.controls['password'].valid) {
            if (this.formGroup.controls['password'].hasError('required')) {
                this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Password is required' });
            }
            else {
                if (this.formGroup.controls['password'].hasError('minlength')) {
                    this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Your password must be at least 6 characters long' });
                }
            }
        }
        if (!this.formGroup.controls['email'].valid) {
            if (this.formGroup.controls['email'].hasError('required')) {
                this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Email is required' });
            }
            else {
                if (this.formGroup.controls['email'].hasError('emailFormat')) {
                    this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Please enter valid email' });
                }
            }
        }
        return false;
    };
    __decorate([
        core_1.Output('loginSubmit'), 
        __metadata('design:type', core_1.EventEmitter)
    ], EntryFormDirective.prototype, "onLoginEmitter", void 0);
    __decorate([
        core_1.Input('responseMsg'), 
        __metadata('design:type', Array)
    ], EntryFormDirective.prototype, "resErrors", void 0);
    EntryFormDirective = __decorate([
        core_1.Component({
            selector: 'entry-form',
            template: "\n\t<div class=\"login-page\">\n\t\t<img src=\"app/assets/img/logo-small.png\" alt=\"\">\n\t\t<form role=\"form\" [ngFormModel]=\"formGroup\" (ngSubmit)=\"onSubmit()\" novalidate>\n\t\t\t<div class=\"form-content\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<input [ngFormControl]=\"email\" type=\"text\" placeholder=\"Email...\">\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<input [ngFormControl]=\"password\" type=\"password\" placeholder=\"Password...\">\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t<p-messages [value]=\"resErrors\"></p-messages>\n\t\t\t\t\n\t\t\t</div>\n            <button type=\"button\" (click)=\"onLogin()\">Log in</button>\n\t\t\t\n\t\t</form>\n\t</div>\n\t<p-growl [value]=\"msgs\" [sticky]=\"true\"></p-growl>\n\t",
            directives: [common_1.FORM_DIRECTIVES, primeng_1.Growl, primeng_1.Messages]
        }), 
        __metadata('design:paramtypes', [common_1.FormBuilder])
    ], EntryFormDirective);
    return EntryFormDirective;
}());
exports.EntryFormDirective = EntryFormDirective;
//# sourceMappingURL=entry-form.directive.js.map