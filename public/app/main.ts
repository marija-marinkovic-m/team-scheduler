import { bootstrap } from '@angular/platform-browser-dynamic';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { provide, enableProdMode, ComponentRef } from '@angular/core';
import { EutelnetAppComponent } from './app.component';
import { ROUTER_PROVIDERS } from '@angular/router-deprecated';
import { FIREBASE_PROVIDERS, defaultFirebase, AngularFire, AuthMethods, AuthProviders, firebaseAuthConfig } from 'angularfire2';

import { appInjector, Auth } from './auth/index';

// enableProdMode();

bootstrap(EutelnetAppComponent, [
    ROUTER_PROVIDERS, 
    provide(LocationStrategy, {useClass: HashLocationStrategy}), 
    FIREBASE_PROVIDERS, 
    defaultFirebase('https://amber-fire-98.firebaseio.com/team-app/'), 
    firebaseAuthConfig({
        provider: AuthProviders.Password, 
        method: AuthMethods.Password
    }), 
    Auth
]).then(
    (appRef:ComponentRef<any>) => {
        appInjector(appRef.injector);
    }, 
    err => console.log(err)
);