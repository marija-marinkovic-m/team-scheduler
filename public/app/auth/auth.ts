import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { FirebaseAuth, FirebaseAuthState } from 'angularfire2';

@Injectable() 
export class Auth {
    constructor(
        private _auth: FirebaseAuth
    ) {}
    
    signup(params: FirebaseCredentials):Promise<FirebaseAuthData> {
        return this._auth.createUser(params);
    }
    
    login(params: FirebaseCredentials):Promise<FirebaseAuthState> {
        return this._auth.login(params);
    }
    
    logout():void {
        this._auth.logout();
    }
    
    state():boolean {
        let authData = this.getData();
        if (authData) {
            return true;
        }
        return false;
    }
    
    getData():FirebaseAuthData {
        return this._auth.getAuth();
    }
}