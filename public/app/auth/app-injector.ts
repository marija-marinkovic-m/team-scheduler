import { Injector } from '@angular/core';

let appInjectorRef:Injector;

export const appInjector = (injector?:Injector) => {
    if (!injector) {
        return appInjectorRef;
    }  
    appInjectorRef = injector;
    return appInjectorRef;
};