import { Injector } from '@angular/core';
import { appInjector } from './app-injector';
import { Auth } from './auth';
import { Router, ComponentInstruction } from '@angular/router-deprecated';

export var authCheck = function(next: ComponentInstruction, prev: ComponentInstruction):Promise<boolean> {
   const injector:Injector = appInjector();
   const auth:Auth = injector.get(Auth);
   const router:Router = injector.get(Router);
 
   return new Promise((resolve, reject) => {
       if (auth.state()) {
           if (next.urlPath == 'login') {
               router.navigate(['/Dashboard']);
               resolve(false);
           } else {
               resolve(true);
           }
       } else {
           // user not logged in 
           if (next.urlPath == 'login') {
               resolve(true);
           } else {
               router.navigate(['/Login']);
               resolve(false);
           }
       }
   });
};