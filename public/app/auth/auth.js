"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angularfire2_1 = require('angularfire2');
var Auth = (function () {
    function Auth(_auth) {
        this._auth = _auth;
    }
    Auth.prototype.signup = function (params) {
        return this._auth.createUser(params);
    };
    Auth.prototype.login = function (params) {
        return this._auth.login(params);
    };
    Auth.prototype.logout = function () {
        this._auth.logout();
    };
    Auth.prototype.state = function () {
        var authData = this.getData();
        if (authData) {
            return true;
        }
        return false;
    };
    Auth.prototype.getData = function () {
        return this._auth.getAuth();
    };
    Auth = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [angularfire2_1.FirebaseAuth])
    ], Auth);
    return Auth;
}());
exports.Auth = Auth;
//# sourceMappingURL=auth.js.map